import os
import random
from twisted.internet import reactor
from twisted.internet.task import LoopingCall
import pygame

FPS = 15
MARGIN = 20
WIDTH_CARD = 120
DISPLAY = pygame.display.set_mode((1900, 1060))
BACKGROUND = (2, 142, 248)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GRAY = (100, 100, 100)


class Game:

    def __init__(self):
        logo = pygame.image.load(os.path.join('images/logo.png'))
        pygame.display.set_icon(logo)
        pygame.display.set_caption("Ahorcado")
        self.cards = []
        self.set_word('DE TAL MANERA')

    def set_word(self, text):
        self.cards = []
        x = 0
        y = 0
        for t in text:
            self.cards.append(Card(t, x, y))
            x += 1

    def display(self):
        DISPLAY.fill(BACKGROUND)
        for c in self.cards:
            c.display()

        pygame.display.flip()


class Card:

    def __init__(self, letter, x, y):
        self.font = pygame.font.SysFont("Arial", 35)
        self.letter = letter
        self.x = x
        self.y = y
        self.rectangle = pygame.Rect(self.get_x_pos(), self.get_y_pos() + 50, WIDTH_CARD, WIDTH_CARD)
        self.show = random.choice([True, False])
        self.tile = pygame.image.load(f'images/tile.png')
        self.text = self.font.render(letter, True, BLACK)

    def get_x_pos(self):
        return MARGIN + (self.x * (WIDTH_CARD + MARGIN))

    def get_y_pos(self):
        return MARGIN + (self.y * (WIDTH_CARD + MARGIN))

    def display(self):
        if self.letter != ' ':
            if self.show:
                pygame.draw.rect(DISPLAY, WHITE, self.rectangle)
                DISPLAY.blit(self.text, (self.get_x_pos(), self.get_y_pos() + 50))
            else:
                pygame.draw.rect(DISPLAY, GRAY, self.rectangle)
                DISPLAY.blit(self.tile, (self.get_x_pos(), self.get_y_pos() + 50))


if __name__ == '__main__':
    pygame.init()
    game = Game()
    clock = LoopingCall(game.display)
    clock.start(1 / FPS)

    reactor.run()
